// console.log("Hello World");

// [SECTION] Arithmetic Operators

    let x = 100;
    let y = 25;

    // Addition
    let sum = x + y;
    console.log("Result of addition operator: " + sum);

    // Subtraction
    let difference = x - y;
    console.log("Result of subtraction operator: " + difference);

    // Multiplication
    let product = x * y;
    console.log("Result of multiplication operator: " + product);

    // Division
    let quotient = x / y;
    console.log("Result of division operator: " + quotient);

    // Modulo
    let modulo = x % y;
    console.log("Result of modulo operator: " + modulo);

// [SECTION] Assignment Operators

    // Basic Assignment Operator (=)
    // The assignment operator assigns the value o fthe "right hand" operand to a variable
    let assignmentNumber = 8;
    console.log("The current value of the assignmentNumber variable: " + assignmentNumber);

    // Addition Assignment Operator
    // assignmentNumber = assignmentNumber + 2; //long method
    assignmentNumber += 2; //shorthand method
    console.log("Result of addition assignmentNumber operator: " + assignmentNumber);

    // Subtraction/Multiplication/Division (-=, *=, /=)
    assignmentNumber -= 2;
    console.log("Result of subtraction assignmentNumber operator: " + assignmentNumber);

    assignmentNumber *= 2;
    console.log("Result of multiplication assignmentNumber operator: " + assignmentNumber);

    assignmentNumber /= 2;
    console.log("Result of division assignmentNumber operator: " + assignmentNumber);

// [SECTION] PEMDAS (Order of Operations)
// Multiple Operators and Parenthesis
// When multiple operators are applied in a single statement, it follows PEMDAS
/* 
    The operations were done in the following order:
    1. 3 * 4 = 12 | 1 + 2 - 12 / 5
    2. 12 / 5 = 2.4 | 1 + 2 - 2.4
    3. 1 + 2 = 3 | 3 - 2.4
    4. 3 - 2.4 = 0.6 //result
*/

    let mdas = 1 + 2 - 3 * 4 / 5;
    console.log("Result of mdas operation: " + mdas);

/* 
    The operations were done in the following order:
    1. 4 / 5 = 0.8 | 1 + (2 -3) * 0.8
    2. 2 - 3 = -1 | 1 + -1 * 0.8
    3. -1 * 0.8 = -0.8 | 1 + -0.8
    4. 1 + -0.8 = 0.2 //result
*/
    // let pemdas = 1 + (2 - 3) * (4 / 5); //.20
    let pemdas = (1 + (2 - 3)) * (4 / 5); 
    console.log("Result of pemdas operation: " + pemdas); //0

// [SECTION] Increment and Decrement
// Operators that add or subtract a value by 1 and reassign the value of the variable where the increment(++)/decrement(--) was applied

    let z = 1;
    let increment = ++z;
    console.log("Result of pre-increment: " + increment);
    console.log("Result of pre-increment for z: " + z);

    increment = z++;
    console.log("Result of post-increment: " + increment);
    console.log("Result of post-increment for z: " + z);

    let decrement = --z;
    console.log("Result of pre-decrement: " + decrement);
    console.log("Result of pre-decrement for z: " + z);
    
    decrement = z--;
    console.log("Result of post-decrement: " + decrement);
    console.log("Result of post-decrement for z: " + z);

// [SECTION] Type Coercion

    let numA = '10';
    let numB = 12;

    let coercion = numA + numB;
    console.log(coercion);
    console.log(typeof coercion);

    let numC = 16;
    let numD = 14;
    let nonCoercion = numC + numD;
    console.log(nonCoercion);
    console.log(typeof nonCoercion);

    /* 
        - The result is a number
        - The boolean "true" is also associated with the value of 1.
        - The boolean "false" is also associated with the value of 0.
    */
    let numE = true + 1;
    console.log(numE);
    console.log(typeof numE);

    let numF = false + 1;
    console.log(numF);
    console.log(typeof numF);

// [SECTION] Comparison Operators
// Comparison operators are used to evaluate and compare the left and right operands
// After evaluation, it returns a boolean value

    let juan = "juan";
    // Equality operator (==)
    /* 
        - Checks whether the operands are equal/have the same content
        - Attempt to convert and compare operands of different data type
    */

    console.log(1 == 1); //true
    console.log(1 == 2); //false
    console.log(1 == '1'); //true
    console.log(0 == false); //true
    console.log('juan' == 'juan'); //true
    console.log('juan' == juan); //true

// Inequality Operator (!=)
/* 
    - Checks whether the operands are not equal/have a different value
    - Attempts to convert and compare operands of different data types
*/
    console.log(1 != 1); //false
    console.log(1 != 2); //true
    console.log(1 != '1'); //false
    console.log(0 != false); //false
    console.log('juan' != 'juan'); //false
    console.log('juan' != juan); //false

// Strict Equality Operator (===)
// Check whether the operands are equal/have the same content
// Also compares the data types of two values
    console.log(1 === 1); //true
    console.log(1 === 2); //false
    console.log(1 === '1'); //false
    console.log(0 === false); //false
    console.log('juan' === 'juan'); //true
    console.log('juan' === juan); //true

// Strict Inequality Operator (!==)
    console.log(1 !== 1); //false
    console.log(1 !== 2); //true
    console.log(1 !== '1'); //true
    console.log(0 !== false); //true
    console.log('juan' !== 'juan'); //false
    console.log('juan' !== juan); //false

// [SECTION] Relational Operators
    let a = 50;
    let b = 65;

// GT or Greater Than Operator (>)
    let isGreaterThan = a > b;
    console.log(isGreaterThan); //false

// LT or Less Than Operator (<)
    let isLessThan = a < b;
    console.log(isLessThan); //true

// GTE or Greater Than or Equal (>=)
    let isGTOrEqual = a >= b;
    console.log(isGTOrEqual); //false

// LTE or Less Than or Equal (<=)
    let isLTOrEqual = a <= b;
    console.log(isLTOrEqual); //true

    let numStr = "30";
    console.log(a > numStr); //true

    let str = "twenty";
    console.log(b >= str); //false

// [SECTION] Logical Operators AND(&&), OR(||), NOT(!)

    let isLegalAge = true;
    let isRegistered = false;

    let allRequirementsMet = isLegalAge && isRegistered;
    console.log("Result of logical AND Operator: " + allRequirementsMet);

    let someRequirementsMet = isLegalAge || isRegistered;
    console.log("Result of logical OR Operator: " + someRequirementsMet);

    let someRequirementsNotMet = !isRegistered;
    console.log("Result of logical OR Operator: " + someRequirementsNotMet);